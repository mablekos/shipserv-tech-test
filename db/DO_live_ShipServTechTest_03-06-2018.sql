# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.01 (MySQL 5.7.12)
# Database: ShipServTechTest
# Generation Time: 2018-06-03 21:14:51 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table accommodation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `accommodation`;

CREATE TABLE `accommodation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `base_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `accommodation` WRITE;
/*!40000 ALTER TABLE `accommodation` DISABLE KEYS */;

INSERT INTO `accommodation` (`id`, `name`, `location`, `base_image`, `created_at`, `updated_at`)
VALUES
	(1,'Apex City of Edinburgh Hotel','61 Grassmarket, Edinburgh, EH1 2JF, United Kingdom','hotel.jpg','2018-06-03 16:25:05','2018-06-03 16:25:05');

/*!40000 ALTER TABLE `accommodation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table accommodation_rooms
# ------------------------------------------------------------

DROP TABLE IF EXISTS `accommodation_rooms`;

CREATE TABLE `accommodation_rooms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `accommodation_id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amenities` mediumtext COLLATE utf8mb4_unicode_ci,
  `base_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `accommodation_rooms_accommodation_id_foreign` (`accommodation_id`),
  CONSTRAINT `accommodation_rooms_accommodation_id_foreign` FOREIGN KEY (`accommodation_id`) REFERENCES `accommodation` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `accommodation_rooms` WRITE;
/*!40000 ALTER TABLE `accommodation_rooms` DISABLE KEYS */;

INSERT INTO `accommodation_rooms` (`id`, `accommodation_id`, `name`, `type`, `amenities`, `base_image`, `created_at`, `updated_at`)
VALUES
	(1,1,'City Double Room','Double Room','24 sqm / 258 sqft\n            Queen bed\n            Bath and/or spacious walk-in shower\n            42’’ LCD TV with Freeview','','2018-06-03 16:25:05','2018-06-03 16:25:05');

/*!40000 ALTER TABLE `accommodation_rooms` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table airline_flights
# ------------------------------------------------------------

DROP TABLE IF EXISTS `airline_flights`;

CREATE TABLE `airline_flights` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `airline_id` int(10) unsigned NOT NULL,
  `flight_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aircraft_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `departure_location_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `departure_location_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `departure_date` timestamp NULL DEFAULT NULL,
  `departure_terminal` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `arrival_location_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `arrival_location_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `arrival_date` timestamp NULL DEFAULT NULL,
  `arrival_terminal` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `airline_flights_airline_id_foreign` (`airline_id`),
  CONSTRAINT `airline_flights_airline_id_foreign` FOREIGN KEY (`airline_id`) REFERENCES `airlines` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `airline_flights` WRITE;
/*!40000 ALTER TABLE `airline_flights` DISABLE KEYS */;

INSERT INTO `airline_flights` (`id`, `airline_id`, `flight_no`, `aircraft_type`, `departure_location_code`, `departure_location_name`, `departure_date`, `departure_terminal`, `arrival_location_code`, `arrival_location_name`, `arrival_date`, `arrival_terminal`, `created_at`, `updated_at`)
VALUES
	(1,1,'EZY239','Airbus A320','STN','Stansted','2018-07-01 20:55:00','5','EDI','Edinburgh','2018-07-01 22:10:00','3','2018-06-03 16:25:05','2018-06-03 16:25:05'),
	(2,1,'EZY20','Airbus A320','EDI','Edinburgh','2018-07-10 20:55:00','5','LTN','Luton','2018-07-10 22:10:00','3','2018-06-03 16:25:05','2018-06-03 16:25:05');

/*!40000 ALTER TABLE `airline_flights` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table airlines
# ------------------------------------------------------------

DROP TABLE IF EXISTS `airlines`;

CREATE TABLE `airlines` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `base_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `airlines` WRITE;
/*!40000 ALTER TABLE `airlines` DISABLE KEYS */;

INSERT INTO `airlines` (`id`, `name`, `location`, `base_image`, `created_at`, `updated_at`)
VALUES
	(1,'Easyjet','London','','2018-06-03 16:25:05','2018-06-03 16:25:05');

/*!40000 ALTER TABLE `airlines` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table car_rentals
# ------------------------------------------------------------

DROP TABLE IF EXISTS `car_rentals`;

CREATE TABLE `car_rentals` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `base_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `car_rentals` WRITE;
/*!40000 ALTER TABLE `car_rentals` DISABLE KEYS */;

INSERT INTO `car_rentals` (`id`, `name`, `location`, `base_image`, `created_at`, `updated_at`)
VALUES
	(1,'Avis','London','','2018-06-03 16:25:05','2018-06-03 16:25:05');

/*!40000 ALTER TABLE `car_rentals` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table car_rentals_vehicles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `car_rentals_vehicles`;

CREATE TABLE `car_rentals_vehicles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `car_rental_id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `included` mediumtext COLLATE utf8mb4_unicode_ci,
  `base_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `car_rentals_vehicles_car_rental_id_foreign` (`car_rental_id`),
  CONSTRAINT `car_rentals_vehicles_car_rental_id_foreign` FOREIGN KEY (`car_rental_id`) REFERENCES `car_rentals` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `car_rentals_vehicles` WRITE;
/*!40000 ALTER TABLE `car_rentals_vehicles` DISABLE KEYS */;

INSERT INTO `car_rentals_vehicles` (`id`, `car_rental_id`, `name`, `type`, `included`, `base_image`, `created_at`, `updated_at`)
VALUES
	(1,1,'Economy 2-3 doors','manual','Fair fuel policy\nAirport terminal pick-up\nUnlimited mileage\nCollision damage waiver\n(Excess: £1,400)\nTheft protection\nThird party cover\nFree cancellation','','2018-06-03 16:25:05','2018-06-03 16:25:05');

/*!40000 ALTER TABLE `car_rentals_vehicles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(1,'2014_10_12_000000_create_users_table',1),
	(2,'2014_10_12_100000_create_password_resets_table',1),
	(3,'2018_06_03_095021_create_user_flights',1),
	(4,'2018_06_03_095113_create_user_car_rentals',2),
	(5,'2018_06_03_095136_create_user_accomodation',3),
	(6,'2018_06_03_100656_create_airline_flights',4),
	(7,'2018_06_03_101128_create_car_rentals',4),
	(8,'2018_06_03_101157_create_accommodation',4),
	(9,'2018_06_03_103101_create_car_rentals_vehicles',4),
	(10,'2018_06_03_104309_create_accommodation_rooms',4),
	(11,'2018_06_03_114316_create_airlines',4),
	(12,'2018_06_03_131219_create_user_itineraries',5),
	(13,'2018_06_03_144107_add_foreign_keys',6);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table user_accommodation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_accommodation`;

CREATE TABLE `user_accommodation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `itinerary_id` int(10) unsigned NOT NULL,
  `accommodation_id` int(10) unsigned NOT NULL,
  `check_in_date` timestamp NULL DEFAULT NULL,
  `check_out_date` timestamp NULL DEFAULT NULL,
  `people_num` int(11) NOT NULL,
  `requirements` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(6,2) DEFAULT NULL,
  `cancellation` tinyint(4) NOT NULL DEFAULT '0',
  `cancellation_reason` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_accommodation_user_id_foreign` (`user_id`),
  KEY `user_accommodation_itinerary_id_foreign` (`itinerary_id`),
  KEY `user_accommodation_accommodation_id_foreign` (`accommodation_id`),
  CONSTRAINT `user_accommodation_accommodation_id_foreign` FOREIGN KEY (`accommodation_id`) REFERENCES `accommodation_rooms` (`id`),
  CONSTRAINT `user_accommodation_itinerary_id_foreign` FOREIGN KEY (`itinerary_id`) REFERENCES `user_itineraries` (`id`),
  CONSTRAINT `user_accommodation_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `user_accommodation` WRITE;
/*!40000 ALTER TABLE `user_accommodation` DISABLE KEYS */;

INSERT INTO `user_accommodation` (`id`, `user_id`, `itinerary_id`, `accommodation_id`, `check_in_date`, `check_out_date`, `people_num`, `requirements`, `price`, `cancellation`, `cancellation_reason`, `created_at`, `updated_at`)
VALUES
	(1,1,1,1,'2018-07-01 10:00:00','2018-07-10 12:00:00',1,'No Smoking',1439.00,0,NULL,'2018-06-03 16:25:05','2018-06-03 16:25:05');

/*!40000 ALTER TABLE `user_accommodation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_car_rentals
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_car_rentals`;

CREATE TABLE `user_car_rentals` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `itinerary_id` int(10) unsigned NOT NULL,
  `vehicle_id` int(10) unsigned NOT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `pickup_location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `dropoff_location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(6,2) DEFAULT NULL,
  `cancellation` tinyint(4) NOT NULL DEFAULT '0',
  `cancellation_reason` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_car_rentals_user_id_foreign` (`user_id`),
  KEY `user_car_rentals_itinerary_id_foreign` (`itinerary_id`),
  KEY `user_car_rentals_vehicle_id_foreign` (`vehicle_id`),
  CONSTRAINT `user_car_rentals_itinerary_id_foreign` FOREIGN KEY (`itinerary_id`) REFERENCES `user_itineraries` (`id`),
  CONSTRAINT `user_car_rentals_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `user_car_rentals_vehicle_id_foreign` FOREIGN KEY (`vehicle_id`) REFERENCES `car_rentals_vehicles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `user_car_rentals` WRITE;
/*!40000 ALTER TABLE `user_car_rentals` DISABLE KEYS */;

INSERT INTO `user_car_rentals` (`id`, `user_id`, `itinerary_id`, `vehicle_id`, `start_date`, `pickup_location`, `end_date`, `dropoff_location`, `price`, `cancellation`, `cancellation_reason`, `created_at`, `updated_at`)
VALUES
	(1,1,1,1,'2018-07-01 21:30:00','Edinburgh Airport','2018-07-10 19:00:00','Edinburgh Airport',400.00,0,NULL,'2018-06-03 16:25:05','2018-06-03 16:25:05');

/*!40000 ALTER TABLE `user_car_rentals` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_flights
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_flights`;

CREATE TABLE `user_flights` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `itinerary_id` int(10) unsigned NOT NULL,
  `flight_id` int(10) unsigned NOT NULL,
  `price` decimal(6,2) DEFAULT NULL,
  `cancellation` tinyint(4) NOT NULL DEFAULT '0',
  `cancellation_reason` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_flights_user_id_foreign` (`user_id`),
  KEY `user_flights_itinerary_id_foreign` (`itinerary_id`),
  KEY `user_flights_flight_id_foreign` (`flight_id`),
  CONSTRAINT `user_flights_flight_id_foreign` FOREIGN KEY (`flight_id`) REFERENCES `airline_flights` (`id`),
  CONSTRAINT `user_flights_itinerary_id_foreign` FOREIGN KEY (`itinerary_id`) REFERENCES `user_itineraries` (`id`),
  CONSTRAINT `user_flights_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `user_flights` WRITE;
/*!40000 ALTER TABLE `user_flights` DISABLE KEYS */;

INSERT INTO `user_flights` (`id`, `user_id`, `itinerary_id`, `flight_id`, `price`, `cancellation`, `cancellation_reason`, `created_at`, `updated_at`)
VALUES
	(1,1,1,1,44.00,0,NULL,'2018-06-03 16:25:05','2018-06-03 16:25:05'),
	(2,1,1,2,44.00,0,NULL,'2018-06-03 16:25:05','2018-06-03 16:25:05');

/*!40000 ALTER TABLE `user_flights` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_itineraries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_itineraries`;

CREATE TABLE `user_itineraries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `departure_location_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `departure_country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `destination_location_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `destination_country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `arrival_date` timestamp NULL DEFAULT NULL,
  `departure_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_itineraries_user_id_foreign` (`user_id`),
  CONSTRAINT `user_itineraries_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `user_itineraries` WRITE;
/*!40000 ALTER TABLE `user_itineraries` DISABLE KEYS */;

INSERT INTO `user_itineraries` (`id`, `user_id`, `departure_location_name`, `departure_country`, `destination_location_name`, `destination_country`, `arrival_date`, `departure_date`, `created_at`, `updated_at`)
VALUES
	(1,1,'London','England','Edinburgh','Scotland','2018-07-01 00:00:00','2018-07-10 00:00:00','2018-06-03 16:25:05','2018-06-03 16:25:05');

/*!40000 ALTER TABLE `user_itineraries` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `username`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(1,'mablekos','Andrew Mablekos','mablekos@gmail.com','$2y$10$gcmbXrj1hAK.oV3ESnq5jOFvmshYbEY6rzr0oNXCq7CrA7ovEtHum',NULL,'2018-06-03 16:25:05','2018-06-03 16:25:05');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
