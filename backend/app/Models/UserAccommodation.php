<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAccommodation extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_accommodation';

    /**
     * Itinerary relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function itinerary(){
        return $this->belongsTo('App\Models\UserItineraries','itinerary_id', 'id');
    }

    /**
     * Accommodation info
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function accommodationInfo(){
        return $this->belongsTo('App\Models\AccommodationRooms', 'accommodation_id', 'id');
    }
}
