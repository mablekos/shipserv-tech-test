<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AirlineFlights extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'airline_flights';

    /**
     * Airline info
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function airline(){
        return $this->belongsTo('App\Models\Airlines', 'airline_id', 'id');
    }
}
