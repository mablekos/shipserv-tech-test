<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarRentalsVehicles extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'car_rentals_vehicles';

    /**
     * Car rental info
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function carRental(){
        return $this->belongsTo('App\Models\CarRentals', 'car_rental_id', 'id');
    }
}
