<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UserItineraries extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_itineraries';


    /**
     * Get user Itineraries
     *
     * @param null $userId
     *
     * @return array $userItineraries
     */
    public function getUserItineraries( $userId = null ){

        $userItineraries = array();

        //get user itineraries
        $userItineraries = $this->where('user_id', $userId)->get();

        //check if user has any itineraries
        if(count($userItineraries) > 0){

            foreach($userItineraries as $userItinerary){

                $userItineraries = array(
                    'id' => $userItinerary->id,
                    'departure_location' => $userItinerary->departure_location_name,
                    'departure_country' => $userItinerary->departure_country,
                    'destination_location' => $userItinerary->destination_location_name,
                    'destination_country' => $userItinerary->destination_country,
                    'departure_date' => Carbon::parse($userItinerary->departure_date)->format('d/m/Y'),
                    'arrival_date' => Carbon::parse($userItinerary->arrival_date)->format('d/m/Y'),
                    'flights' => array(),
                    'hotels' => array(),
                    'cars' => array()
                );

                //get user flights
                $userItineraryFlights = $this->find($userItinerary->id)->flights;

                //get flight info for each flight
                if(count($userItineraryFlights) > 0) {
                    foreach ($userItineraryFlights as $userItineraryFlight) {
                        $userItineraryFlightInfo = $userItineraryFlight->flightInfo;

                        $userItineraries['flights'][] = array(
                            'airline' => $userItineraryFlightInfo->airline->name,
                            'flight_no' => $userItineraryFlightInfo->flight_no,
                            'aircraft_type' => $userItineraryFlightInfo->aircraft_type,
                            'departure_location_code' => $userItineraryFlightInfo->departure_location_code,
                            'departure_location_name' => $userItineraryFlightInfo->departure_location_name,
                            'departure_date' => Carbon::parse($userItineraryFlightInfo->departure_date)->format('d/m/Y H:m:s'),
                            'departure_terminal' => $userItineraryFlightInfo->departure_terminal,
                            'arrival_location_code' => $userItineraryFlightInfo->arrival_location_code,
                            'arrival_location_name' => $userItineraryFlightInfo->arrival_location_name,
                            'arrival_date' => Carbon::parse($userItineraryFlightInfo->arrival_date)->format('d/m/Y H:m:s'),
                            'arrival_terminal' => $userItineraryFlightInfo->arrival_terminal,
                            'price' => $userItineraryFlight->price
                        );
                    }
                }

                //get user accommodation bookings
                $userItineraryAccommodations = $this->find($userItinerary->id)->accommodation;

                //get accommodation info for each booking
                if(count($userItineraryAccommodations) > 0){

                    foreach ($userItineraryAccommodations as $userItineraryAccommodation){

                        $userItineraryAccommodationInfo = $userItineraryAccommodation->accommodationInfo;

                        $userItineraries['hotels'][] = array(
                            'hotel_name' => $userItineraryAccommodationInfo->accommodation->name,
                            'hotel_location' => $userItineraryAccommodationInfo->accommodation->location,
                            'hotel_base_image' => $userItineraryAccommodationInfo->accommodation->base_image,
                            'room_name' => $userItineraryAccommodationInfo->name,
                            'room_type' => $userItineraryAccommodationInfo->type,
                            'room_amenities' => $userItineraryAccommodationInfo->amenities,
                            'room_base_image' => $userItineraryAccommodationInfo->base_image,
                            'check_in_date' => Carbon::parse($userItineraryAccommodation->check_in_date)->format('d/m/Y'),
                            'check_out_date' => Carbon::parse($userItineraryAccommodation->check_out_date)->format('d/m/Y'),
                            'people_num' => $userItineraryAccommodation->people_num,
                            'requirements' => $userItineraryAccommodation->requirements,
                            'price' => $userItineraryAccommodation->price
                        );

                    }

                }

                //get user car rentals
                $userItineraryCarRentals = $this->find($userItinerary->id)->car;

                //get car rental info for each booking
                if(count($userItineraryCarRentals) > 0){

                    foreach ($userItineraryCarRentals as $userItineraryCarRental){

                        $userItineraryCarRentalInfo = $userItineraryCarRental->vehicleInfo;

                        $userItineraries['cars'][] = array(
                            'car_rental_company' => $userItineraryCarRentalInfo->carRental->name,
                            'car_rental_location' => $userItineraryCarRentalInfo->carRental->location,
                            'vehicle_name' => $userItineraryCarRentalInfo->name,
                            'vehicle_type' => $userItineraryCarRentalInfo->type,
                            'vehicle_included' => $userItineraryCarRentalInfo->included,
                            'vehicle_base_image' => $userItineraryCarRentalInfo->base_image,
                            'car_rental_start_date' => Carbon::parse($userItineraryCarRental->start_date)->format('d/m/Y'),
                            'car_rental_pickup_location' => $userItineraryCarRental->pickup_location,
                            'car_rental_end_date' => Carbon::parse($userItineraryCarRental->end_date)->format('d/m/Y'),
                            'car_rental_dropoff_location' => $userItineraryCarRental->dropoff_location,
                            'car_rental_price' => $userItineraryCarRental->price
                        );

                    }

                }

            }

        }

        return $userItineraries;

    }

    /**
     * User flights
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function flights(){

        return $this->hasMany('App\Models\UserFlights', 'itinerary_id', 'id');

    }

    /**
     * User accommodations
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function accommodation(){

        return $this->hasMany('App\Models\UserAccommodation', 'itinerary_id', 'id');
    }

    /**
     * User Car rentals
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function car(){

        return $this->hasMany('App\Models\UserCarRentals', 'itinerary_id', 'id');
    }



}
