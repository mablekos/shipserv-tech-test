<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarRentals extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'car_rentals';
}
