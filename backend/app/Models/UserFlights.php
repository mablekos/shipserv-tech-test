<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserFlights extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_flights';

    /**
     * Itinerary relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function itinerary(){
        return $this->belongsTo('App\Models\UserItineraries','itinerary_id', 'id');
    }

    /**
     * Flight info
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function flightInfo(){
        return $this->belongsTo('App\Models\AirlineFlights', 'flight_id', 'id');
    }
}
