<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCarRentals extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_car_rentals';

    /**
     * Itinerary relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function itinerary(){
        return $this->belongsTo('App\Models\UserItineraries','itinerary_id', 'id');
    }

    /**
     * Vehicle info
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function vehicleInfo(){
        return $this->belongsTo('App\Models\CarRentalsVehicles', 'vehicle_id', 'id');
    }
}
