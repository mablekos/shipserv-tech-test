<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccommodationRooms extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'accommodation_rooms';

    /**
     * Accomodation info
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function accommodation(){
        return $this->belongsTo('App\Models\Accommodation', 'accommodation_id', 'id');
    }
}
