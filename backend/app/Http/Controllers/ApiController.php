<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\UserItineraries;
use App\Models\User;
use Illuminate\Http\Request;

class ApiController extends Controller
{

    /**
     * get User Itinerary
     *
     * @param Request $request
     *
     * @return json array with user itineraries
     */
    public function getUserItinerary(Request $request, UserItineraries $userItinerariesModel, $username = ''){


        // validate request for including username as a required field
        // and that username might include only alpha-numeric characters, as well as dashes and underscores.
        $validator = Validator::make(
            [
                'username' => $username
            ],
            [
                'username' => ['required', 'alpha_dash']
            ],
            [
                'username.required' => 'username is required',
                'username.alpha_dash' => 'username is not valid'
            ]
        );

        //validation has failed
        if ($validator->fails()) {

            return response()->json([
                'error' => $validator->errors()
            ]);

        }else{
            //get user details
            $userModel = User::where('username', $username)->get();

            //user exists
            if(count($userModel) > 0){

                //get user Itineraries

                $userItineraries = $userItinerariesModel->getUserItineraries($userModel[0]->id);

                return response()->json([
                    'userItineraries'  => $userItineraries
                ]);

            }else{ //user does not exist in the database
                return response()->json([
                    'error'  => 'user does not exist in the Database'
                ]);
            }

        }

    }





}
