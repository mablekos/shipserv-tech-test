<?php

use Illuminate\Http\Request;
use App\Http\Middleware\Cors;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// get user itineraries
Route::get('/itineraries/{username}', 'ApiController@getUserItinerary')->middleware(Cors::class);
