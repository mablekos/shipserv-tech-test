<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * user flights foreign keys
         */
        Schema::table('user_flights', function (Blueprint $table) {
           $table->foreign('user_id')->references('id')->on('users');
           $table->foreign('itinerary_id')->references('id')->on('user_itineraries');
           $table->foreign('flight_id')->references('id')->on('airline_flights');
        });

        /**
         * user car rentals foreign keys
         */
        Schema::table('user_car_rentals', function (Blueprint $table) {
           $table->foreign('user_id')->references('id')->on('users');
           $table->foreign('itinerary_id')->references('id')->on('user_itineraries');
           $table->foreign('vehicle_id')->references('id')->on('car_rentals_vehicles');
        });

        /**
         * User Accomodation foreign keys
         */
        Schema::table('user_accommodation', function (Blueprint $table) {
          $table->foreign('user_id')->references('id')->on('users');
          $table->foreign('itinerary_id')->references('id')->on('user_itineraries');
          $table->foreign('accommodation_id')->references('id')->on('accommodation_rooms');
        });

        /**
         * Airline Flights foreign keys
         */
        Schema::table('airline_flights', function (Blueprint $table) {
           $table->foreign('airline_id')->references('id')->on('airlines');
        });

        /**
         * Car rental vehicles foreign keys
         */
        Schema::table('car_rentals_vehicles', function (Blueprint $table) {
           $table->foreign('car_rental_id')->references('id')->on('car_rentals');
        });

        /**
         * Accommodation rooms foreign keys
         */
        Schema::table('accommodation_rooms', function (Blueprint $table) {
           $table->foreign('accommodation_id')->references('id')->on('accommodation');
        });

        /**
         * User Itineraries foreign keys
         */
        Schema::table('user_itineraries', function (Blueprint $table) {
           $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
