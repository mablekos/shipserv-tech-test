<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccommodationRooms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accommodation_rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('accommodation_id');
           // $table->foreign('accommodation_id')->references('id')->on('accommodation');
            $table->string('name');
            $table->string('type');
            $table->mediumText('amenities');
            $table->string('base_image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
