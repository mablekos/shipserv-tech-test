<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserItineraries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * User Itineraries
         */
        Schema::create('user_itineraries', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
           // $table->foreign('user_id')->references('id')->on('users');
            $table->string('departure_location_name');
            $table->string('departure_country');
            $table->string('destination_location_name');
            $table->string('destination_country');
            $table->timestamp('arrival_date')->nullable();
            $table->timestamp('departure_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
