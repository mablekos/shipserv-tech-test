<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAirlineFlights extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Flights table
         */
        Schema::create('airline_flights', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('airline_id');
           // $table->foreign('airline_id')->references('id')->on('airlines');
            $table->string('flight_no');
            $table->string('aircraft_type');
            $table->string('departure_location_code');
            $table->string('departure_location_name');
            $table->timestamp('departure_date')->nullable();
            $table->string('departure_terminal');
            $table->string('arrival_location_code');
            $table->string('arrival_location_name');
            $table->timestamp('arrival_date')->nullable();
            $table->string('arrival_terminal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
