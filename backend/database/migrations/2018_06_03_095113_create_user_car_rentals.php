<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCarRentals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * User Car Rentals table
         */
        Schema::create('user_car_rentals', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
           // $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedInteger('itinerary_id');
          //  $table->foreign('itinerary_id')->references('id')->on('user_itineraries');
            $table->unsignedInteger('vehicle_id');
           // $table->foreign('vehicle_id')->references('id')->on('car_rentals_vehicles');
            $table->timestamp('start_date')->nullable();
            $table->string('pickup_location');
            $table->timestamp('end_date')->nullable();
            $table->string('dropoff_location');
            $table->decimal('price',6,2);
            $table->tinyInteger('cancellation')->default(0);
            $table->mediumText('cancellation_reason')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
