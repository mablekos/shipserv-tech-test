<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserFlights extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Use Flights table
         */
        Schema::create('user_flights', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
           // $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedInteger('itinerary_id');
          //  $table->foreign('itinerary_id')->references('id')->on('user_itineraries');
            $table->unsignedInteger('flight_id');
           // $table->foreign('flight_id')->references('id')->on('airline_flights');
            $table->decimal('price',6,2);
            $table->tinyInteger('cancellation')->default(0);
            $table->mediumText('cancellation_reason')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
