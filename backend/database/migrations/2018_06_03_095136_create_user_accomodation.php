<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAccomodation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * User Accomodation
         */
        Schema::create('user_accommodation', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
          //  $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedInteger('itinerary_id');
          //  $table->foreign('itinerary_id')->references('id')->on('user_itineraries');
            $table->unsignedInteger('accommodation_id');
          //  $table->foreign('accommodation_id')->references('id')->on('accommodation_rooms');
            $table->timestamp('check_in_date')->nullable();
            $table->timestamp('check_out_date')->nullable();
            $table->integer('people_num');
            $table->mediumText('requirements');
            $table->decimal('price',6,2);
            $table->tinyInteger('cancellation')->default(0);
            $table->mediumText('cancellation_reason')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
