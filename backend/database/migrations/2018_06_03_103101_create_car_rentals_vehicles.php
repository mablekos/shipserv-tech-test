<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarRentalsVehicles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_rentals_vehicles', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('car_rental_id');
           // $table->foreign('car_rental_id')->references('id')->on('car_rentals');
            $table->string('name');
            $table->string('type');
            $table->mediumText('included');
            $table->string('base_image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
