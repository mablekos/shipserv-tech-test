<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UserItinerariesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_itineraries')->insert([
            'user_id' => '1',
            'departure_location_name' => 'London',
            'departure_country' => 'England',
            'destination_location_name' => 'Edinburgh',
            'destination_country' => 'Scotland',
            'arrival_date' => '2018-07-01 00:00:00',
            'departure_date' => '2018-07-10 00:00:00',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
