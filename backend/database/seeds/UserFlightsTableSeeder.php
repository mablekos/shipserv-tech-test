<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UserFlightsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_flights')->insert([
            'user_id' => '1',
            'itinerary_id' => '1',
            'flight_id' => '1',
            'price' => '44',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('user_flights')->insert([
            'user_id' => '1',
            'itinerary_id' => '1',
            'flight_id' => '2',
            'price' => '44',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
