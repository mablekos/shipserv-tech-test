<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UsersTableSeeder::class,
            AccommodationTableSeeder::class,
            AccommodationRoomsTableSeeder::class,
            AirlinesTableSeeder::class,
            AirlineFlightsTableSeeder::class,
            CarRentalsTableSeeder::class,
            CarRentalsVehiclesTableSeeder::class,
            UserItinerariesTableSeeder::class,
            UserAccommodationTableSeeder::class,
            UserCarRentalsTableSeeder::class,
            UserFlightsTableSeeder::class
        ]);
    }
}
