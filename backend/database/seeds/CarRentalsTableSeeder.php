<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CarRentalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('car_rentals')->insert([
            'name' => 'Avis',
            'location' => 'London',
            'base_image' => '',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
