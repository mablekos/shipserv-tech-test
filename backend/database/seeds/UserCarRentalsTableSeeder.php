<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UserCarRentalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_car_rentals')->insert([
            'user_id' => '1',
            'itinerary_id' => '1',
            'vehicle_id' => '1',
            'start_date' => '2018-07-01 21:30:00',
            'pickup_location' => 'Edinburgh Airport',
            'end_date' => '2018-07-10 19:00:00',
            'dropoff_location' => 'Edinburgh Airport',
            'price' => '400',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
