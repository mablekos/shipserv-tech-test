<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AirlineFlightsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('airline_flights')->insert([
            'airline_id' => '1',
            'flight_no' => 'EZY239',
            'aircraft_type' => 'Airbus A320',
            'departure_location_code' => 'STN',
            'departure_location_name' => 'Stansted',
            'departure_terminal' => '5',
            'departure_date' => '2018-07-01 20:55:00',
            'arrival_location_code' => 'EDI',
            'arrival_location_name' => 'Edinburgh',
            'arrival_date' => '2018-07-01 22:10:00',
            'arrival_terminal' => '3',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('airline_flights')->insert([
            'airline_id' => '1',
            'flight_no' => 'EZY20',
            'aircraft_type' => 'Airbus A320',
            'departure_location_code' => 'EDI',
            'departure_location_name' => 'Edinburgh',
            'departure_terminal' => '5',
            'departure_date' => '2018-07-10 20:55:00',
            'arrival_location_code' => 'LTN',
            'arrival_location_name' => 'Luton',
            'arrival_date' => '2018-07-10 22:10:00',
            'arrival_terminal' => '3',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
