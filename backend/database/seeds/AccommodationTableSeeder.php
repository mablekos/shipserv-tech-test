<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AccommodationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('accommodation')->insert([
            'name' => 'Apex City of Edinburgh Hotel',
            'location' => '61 Grassmarket, Edinburgh, EH1 2JF, United Kingdom',
            'base_image' => 'hotel.jpg',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
