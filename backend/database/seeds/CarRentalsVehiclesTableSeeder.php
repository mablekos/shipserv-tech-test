<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CarRentalsVehiclesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('car_rentals_vehicles')->insert([
            'car_rental_id' => '1',
            'name' => 'Economy 2-3 doors',
            'type' => 'manual',
            'included' => 'Fair fuel policy
            Airport terminal pick-up
            Unlimited mileage
            Collision damage waiver
            (Excess: £1,400)
            Theft protection
            Third party cover
            Free cancellation',
            'base_image' => '',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
