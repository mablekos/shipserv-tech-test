<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UserAccommodationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('user_accommodation')->insert([
            'user_id' => '1',
            'itinerary_id' => '1',
            'accommodation_id' => '1',
            'check_in_date' => '2018-07-01 10:00:00',
            'check_out_date' => '2018-07-10 12:00:00',
            'people_num' => '1',
            'requirements' => 'No Smoking',
            'price' => '1439',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
