<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AccommodationRoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('accommodation_rooms')->insert([
            'accommodation_id' => '1',
            'name' => 'City Double Room',
            'type' => 'Double Room',
            'amenities' => '24 sqm / 258 sqft
            Queen bed
            Bath and/or spacious walk-in shower
            42’’ LCD TV with Freeview',
            'base_image' => '',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
