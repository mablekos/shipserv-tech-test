<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TestApi extends TestCase
{
    /**
     * Test that the API is accessible.
     *
     * @return void
     */
    public function testApiAccessible()
    {
        $response = $this->get('/api/itineraries/mablekos');

        $response->assertStatus(200);
    }

    public function testApiReturnsJSON()
    {
        $response = $this->json('GET', '/api/itineraries/mablekos');

        $response->assertStatus(200)
        ->assertJson([
            'userItineraries' => array('departure_location' => 'London')
        ]);
    }

    public function testApiReturnsValidJSONStructure(){

        $response = $this->json('GET', '/api/itineraries/mablekos');

        $response->assertStatus(200)
            ->assertJson([
                'userItineraries' => array(
                    'flights' => array(),
                    'hotels' => array(),
                    'cars' => array()
                ),

            ]);

    }

}
