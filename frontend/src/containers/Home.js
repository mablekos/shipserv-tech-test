import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import TableView from '../components/tableView';

class Home extends Component{

    constructor(props) {
        super(props);
        this.state = {
            flights: [],
            hotels: [],
            cars: []
        };
    }
    
    componentDidMount = () => {

        var customHeaders = new Headers({
            'Content-Type': 'application/json'
        });

        fetch('http://shipservtechtest.bbdperfectstorm.net/api/itineraries/mablekos',{
            headers: customHeaders,
            cache: 'no-cache',
            credentials: 'same-origin',
        }).then(response => {
          //  debugger;
            return response.json();
        }).then(data => {
            let flights = data.userItineraries.flights;
            let hotels = data.userItineraries.hotels;
            let cars = data.userItineraries.cars;

            this.setState({flights: flights, hotels: hotels, cars: cars});
          
        }).catch( error => {
            console.log(error);
            }
        )

    }

    render(){

        const styles = theme => ({
            root: {
                width:'100%',
                marginTop: 100,
                overflowX: 'auto'
            },
            table:{
                minWidth: 700,
            },
        });
        
        const HomeWrapper = styled.div`
            max-width: 90vw;
            min-height: 100vh;
            margin:0 auto;
            margin-top:50px;
        `;

        const { classes } = this.props;

        return (
            <HomeWrapper>
                <Paper className={classes.root}>
                    <TableView tableType="flights" data={this.state.flights}></TableView>
                    <TableView tableType="hotels" data={this.state.hotels}></TableView>
                    <TableView tableType="cars" data={this.state.cars}></TableView>
                </Paper>
            </HomeWrapper>
        )

        Home.propTypes = {
            classes: PropTypes.object.isRequired,
            itinerary: PropTypes.object
        };
    }

}

export default withStyles(this.styles)(Home);

