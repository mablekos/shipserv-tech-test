import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import TableCell from '@material-ui/core/TableCell';
import './tableCellView.css';

const TableCellView = (props) => {
     return (
           <TableCell colSpan={props.colSpan} className="no-padding" style={props.style} >{props.text}</TableCell>
      );
}

TableCellView.propTypes = {
    cells: PropTypes.object
}

export default TableCellView;