import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Table from '@material-ui/core/Table';
import TableRow from '@material-ui/core/TableRow';
import TableCellView from './tableCellView';
import TableBody from '@material-ui/core/TableBody';

const TableViewStyled = styled.table`
    padding-top:20px;
    margin-bottom:20px;
    width: 100%;
`;

const cellHeaderStyle = {
    backgroundColor: '#a1a1b5',
    marginLeft: 20,
};

const TableView = (props) => {

    let tablehead = null;
    let headerTitles = [];

    switch(props.tableType){
        case 'flights':
            headerTitles = ['Airline','Flight','Type','Departure From','Date','Terminal','Destination','Date'];
            break;
        case 'hotels':
            headerTitles = ['Hotel Name','Room Name','Type','Amenities','Check-in date','Check-out date'];
            break;
        case 'cars':
            headerTitles = ['Company','Vehicle','Type','Start date','Pick-up location','End date','Drop-off location'];
            break;
        default:
            headerTitles = [];    
    }

    let TableHeader = null;

    if(headerTitles.length > 0){
       
        TableHeader = (
            <TableRow>
            {
                headerTitles.map((headerText, index)=>{
                    return <TableCellView text={headerText} key={index}/>;
                })
            }
           </TableRow>
        );
    }

    const { classes } = props;

    return (
        <TableViewStyled>
            {
               (props.tableType === 'flights')? 
                   <TableRow>
                        <TableCellView text="Flights" colSpan={8} style={cellHeaderStyle}></TableCellView>
                   </TableRow>
                   :''
           }
           {
               (props.tableType === 'hotels')? 
                   <TableRow>
                        <TableCellView text="Hotels" colSpan={6} style={cellHeaderStyle}></TableCellView>
                   </TableRow>
                   :''
           }
           {
               (props.tableType === 'cars')? 
                   <TableRow>
                        <TableCellView text="Car Rentals" colSpan={7} style={cellHeaderStyle}></TableCellView>
                   </TableRow>
                   :''
           }
            {TableHeader}
            <TableBody>
            {
                (props.data.length > 0)
                ?
                    props.data.map((dataInfo,index) => {
                        switch(props.tableType){
                            case 'flights':
                            return (
                                <TableRow>
                                    <TableCellView text={dataInfo.airline}></TableCellView>
                                    <TableCellView text={dataInfo.flight_no}></TableCellView>
                                    <TableCellView text={dataInfo.aircraft_type}></TableCellView>
                                    <TableCellView text={dataInfo.departure_location_code+" / "+dataInfo.departure_location_name}></TableCellView>
                                    <TableCellView text={dataInfo.departure_date}></TableCellView>
                                    <TableCellView text={dataInfo.departure_terminal}></TableCellView>
                                    <TableCellView text={dataInfo.arrival_location_code+" / "+dataInfo.arrival_location_name}></TableCellView>
                                    <TableCellView text={dataInfo.arrival_date}></TableCellView>
                                </TableRow>
                            );
                            break;
                            case 'hotels':
                            return (
                                <TableRow>
                                    <TableCellView text={dataInfo.hotel_name}></TableCellView>
                                    <TableCellView text={dataInfo.room_name}></TableCellView>
                                    <TableCellView text={dataInfo.room_type}></TableCellView>
                                    <TableCellView text={dataInfo.room_amenities}></TableCellView>
                                    <TableCellView text={dataInfo.check_in_date}></TableCellView>
                                    <TableCellView text={dataInfo.check_out_date}></TableCellView>
                                </TableRow>  
                            )
                            break;
                            case 'cars':
                            return (

                                <TableRow>
                                    <TableCellView text={dataInfo.car_rental_company}></TableCellView>
                                    <TableCellView text={dataInfo.vehicle_name}></TableCellView>
                                    <TableCellView text={dataInfo.vehicle_type}></TableCellView>
                                    <TableCellView text={dataInfo.car_rental_start_date}></TableCellView>
                                    <TableCellView text={dataInfo.car_rental_pickup_location}></TableCellView>
                                    <TableCellView text={dataInfo.car_rental_end_date}></TableCellView>
                                    <TableCellView text={dataInfo.car_rental_dropoff_location}></TableCellView>
                                </TableRow>    
                            )
                            break;
                        }
                    })
                :''
            }
            </TableBody>
       </TableViewStyled>
    );

}

Table.PropTypes = {
    tableType: PropTypes.string,
    data: PropTypes.object
}

export default TableView;