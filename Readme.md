## ShipServ Tech Test

### Goal

Create API end points to view the data for the user with a front-end using react components to view the data for the user, to view their bookings, itineraries, and other important info along with a simple UI for the user to view that information.

### Project URLs 

  * [User Itinerary](http://shipservtechtest.bbdperfectstorm.net/itineraries) Itinerary page with the Flights, Hotels and car rentals for the user
  * [API results](http://shipservtechtest.bbdperfectstorm.net/api/itineraries/mablekos) API response in JSON format

### Project Paths

- Back-end

    ```
    backend/
    ```

- Front-end

    ```
    frontend/
    ```

- DB Export and Schema

    ```
    db/
    ```

- Itinerary example testcase

    ```
    itineraryData    
    ```


### Technology


The project includes two parts:

#### Back-end

  The API part of the application was created using the laravel framework
  It has an endpoint which accepts one parameter, the username.
  
  The following URL displays the response from the API 
  
  [http://shipservtechtest.bbdperfectstorm.net/api/itineraries/mablekos](http://shipservtechtest.bbdperfectstorm.net/api/itineraries/mablekos)
  
  For testing purposes there is only one user in Database with a username "mablekos"
  
  The JSON response includes information about the user's itineraries
  which include Flights,Accommodation and Car Rentals for a journey from London to Edinburgh.
  
 
  Technologies used:
  
  * [Laravel Framework](https://laravel.com/) (Latest version) for handling the framework for the API
  * [Laravel Controlers](https://laravel.com/docs/5.6/controllers) for handling the API controller
  
    The API controller can be found under
    
    ```
    app/
     Http/
        Controllers/
            ApiController.php
    ```
  
  * [Laravel Eloquent ORM](https://laravel.com/docs/5.6/eloquent) For handling the Database queries
  
    All Eloquent models can be found under
    
    ```
    backend/
      app/
        Models/
    ```
    
  * [Laravel Validation](https://laravel.com/docs/5.6/validation) For handling the validation of the data passed to the API

    The validation logic can be found under
    
    ```
    backend/
      app/
       Http/
        Controllers/
          ApiController.php
    ```
    
  * [Laravel Migrations](https://laravel.com/docs/5.6/migrations) For handling the database schema
    
    All database migrations can be found under
    
    ```
    backend/
      database/
        migrations/
    ```
  * [Laravel Seeding](https://laravel.com/docs/5.6/seeding) As a method to seed the database with test values
  
    All seed classes can be found under
    
    ```
    backend/
      database/
        seeds/
    ```
  
  * [Laravel Routing](https://laravel.com/docs/5.6/routing) To handle the routes for the API and the front end part of the application
  
    The API routes can be found under 
    
    ```
    backend/
      routes/
        api.php
    ```
    
    and the routes related to the front-end part of the application can be found under
    
    ```
    backend/
      routes/
        web.php
    ```
  
  * [Laravel PHPUnit Testing](https://laravel.com/docs/5.6/http-tests) To create some basic Unit tests to check the validity of the API JSON response
  
    The tests can be found under
    
    ```
    backend/
      tests/
        Unit/
          TestApi.php
    ```

TODO:

A JWT access token functionality should be implemented for the app to be more secure.

#### Database

I used MySQL as a data storage mechanism with the following tables included in the DB Schema

    ```
    - users
    
    includes information about the users
    
    - user_itineraries
    
    holds information about the user itineraries
    
    - user_flights
    
    holds information about the user booked flight related to an itinerary
    
    - user_car_rentals
    
    holds information about the user car rentals related to an itinerary
    
    - user_accommodation
    
    holds information about the user hotel bookings related to an itinerary
    
    - password_resets
    
    holds information about user password reset requests (default table provided by laravel)
        
    - airlines
    
    holds information about Airlines
    
    - airline_flights
    
    hold information about Flights related to Airlines
    
    - car_rentals
    
    holds information about car rental companies
    
    - car_rentals_vehicles
    
    holds information about the vehicles each rental company has
    
    - accommodation
    
    holds information about hotels
    
    - accommodation_rooms
    
    holds information about rooms related to hotels
    
    ```

    an export and an image with the schema of the Database can be found in 
    
    ```
    db/      
    ```

#### Front-end      

I used React to create a component which displays the 3 different types (flights, accomodation, car rental) of bookings for the user itinerary. 

Technologies used:

   * [create-react-app](https://github.com/facebook/create-react-app) for handling the React App scaffolding and framework.

     The compiled files can be found under 
     
        ```
        backend/
          public/
            dashboard/
        ```
   * [React style components](https://www.styled-components.com/) to apply some styling to each component used in the application     
   * [Material UI](https://material-ui.com/) to apply the generic styling and use the Material-UI components to display the bookings
   
     All source files for front-end can be found under
     
     ```
     frontend/
       src/
     ```

TODO:

Better styling needs to be applied and options for the user to handle the itineraries

     